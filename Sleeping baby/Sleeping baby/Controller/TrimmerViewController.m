//
//  TrimmerViewController.m
//  Sleeping baby
//
//  Created by Beautistar on 1/11/17.
//  Copyright © 2017 beautistar. All rights reserved.
//

#import "TrimmerViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface TrimmerViewController () {
    
    __weak IBOutlet UIImageView *imvBorder;
    BOOL isTapped;
    AVAudioPlayer *audioPlayer;
}

@end

@implementation TrimmerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/HairTrimmer.mp3", [[NSBundle mainBundle] resourcePath]]];
    
    NSError *error;
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    audioPlayer.numberOfLoops = -1;
    audioPlayer.volume = 1.0;
    
    if (audioPlayer == nil)
        NSLog(@"%@", [error description]);
    else
        [audioPlayer prepareToPlay];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    isTapped = NO;
    
    [self setView];
}


- (IBAction)didTap:(id)sender {
    
    isTapped = !isTapped;
    
    [self setView];
    
    [self play];
}

- (void) setView {
    
    if (isTapped) {
        
        [imvBorder setImage:[UIImage imageNamed:@"border_green"]];
        
    } else {
        
        [imvBorder setImage:[UIImage imageNamed:@"border_red"]];
    }
}

- (void) play {
    
    if ([audioPlayer isPlaying]) {
        
        [audioPlayer pause];
    } else {
        
        [audioPlayer play];
    }
}

- (void) viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [audioPlayer stop];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];
    
    isTapped = NO;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
