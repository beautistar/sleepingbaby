//
//  main.m
//  Sleeping baby
//
//  Created by Beautistar on 1/11/17.
//  Copyright © 2017 beautistar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
